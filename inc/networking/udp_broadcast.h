#pragma once

class UDPBCPublisher
{
private:
  int _sock;

public:
  UDPBCPublisher(UDPBCPublisher const&) = delete;
  UDPBCPublisher(char const* local_addr, char const* bc_addr, int dest_port);
  ~UDPBCPublisher();
  bool send(void const* data, int sz, bool blocking=false);
};

class UDPBCSubscriber
{
private:
  int _sock;

public:
  UDPBCSubscriber(UDPBCSubscriber const&) = delete;
  UDPBCSubscriber(char const* local_addr, int local_port);
  ~UDPBCSubscriber();
  bool is_buffer_empty();
  int get_last(void* data, int sz);
  int read_next(void* data, int sz, bool blocking=true);
};
