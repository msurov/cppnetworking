#include <cppmisc/throws.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <cppmisc/traces.h>
#include <cppmisc/timing.h>
#include <networking/udp_broadcast.h>


static sockaddr_in make_addr(char const* addr, int port=0)
{
  sockaddr_in ap;
  memset(&ap, 0, sizeof(ap));
  ap.sin_family = AF_INET;
  if (port < 0 || port > std::numeric_limits<decltype(ap.sin_port)>::max())
    throw_invalid_argument("incorrect port: ", port);
  ap.sin_port = htons(port);
  ap.sin_addr.s_addr = INADDR_ANY;
  ap.sin_addr.s_addr = inet_addr(addr);
  return ap;
}

UDPBCPublisher::UDPBCPublisher(char const* local_addr, char const* bc_addr, int dest_port)
{
  int status;
  sockaddr_in adr_srvr = make_addr(local_addr);
  sockaddr_in adr_bc = make_addr(bc_addr, dest_port);

  _sock = socket(AF_INET, SOCK_DGRAM, 0);
  if (_sock < 0)
    throw_runtime_error("socket: ", strerror(errno));

  int so_broadcast = 1;
  status = setsockopt(_sock, SOL_SOCKET, SO_BROADCAST, &so_broadcast, sizeof(so_broadcast));
  if (status < 0)
  {
    close(_sock);
    _sock = -1;
    throw_runtime_error("setsockopt SO_BROADCAST: ", strerror(errno));
  }
  int len_srvr = sizeof(adr_srvr);
  status = bind(_sock, reinterpret_cast<sockaddr*>(&adr_srvr), len_srvr);
  if (status < 0)
  {
    close(_sock);
    _sock = -1;
    throw_runtime_error("bind:", strerror(errno));
  }

  status = connect(_sock, (sockaddr const*)&adr_bc, sizeof(adr_bc));
  if (status < 0)
  {
    close(_sock);
    _sock = -1;
    throw_runtime_error("connect:", strerror(errno));
  }
}

UDPBCPublisher::~UDPBCPublisher()
{
  if (_sock >= 0)
    close(_sock);
  _sock = -1;
}

bool UDPBCPublisher::send(void const* data, int sz, bool blocking)
{
  if (blocking)
  {
    int n = ::send(_sock, data, sz, 0);
    return n == sz;
  }
  else
  {
    int n = ::send(_sock, data, sz, MSG_DONTWAIT);
    return one_of(n, {sz, EAGAIN, EWOULDBLOCK});
  }
}

UDPBCSubscriber::UDPBCSubscriber(char const* local_addr, int local_port)
{
  int status;
  sockaddr_in addr = make_addr(local_addr, local_port);
  socklen_t addr_len = sizeof(addr);

  _sock = socket(AF_INET, SOCK_DGRAM, 0);
  if (_sock < 0)
    throw_runtime_error("socket: ", strerror(errno));

  int so_reuseaddr = 1;
  status = setsockopt(_sock, SOL_SOCKET, SO_REUSEADDR, &so_reuseaddr, sizeof(so_reuseaddr));
  if (status < 0)
  {
    close(_sock);
    _sock = -1;
    throw_runtime_error("setsockopt SO_REUSEADDR: ", strerror(errno));
  }
  
  status = bind(_sock, reinterpret_cast<sockaddr*>(&addr), addr_len);
  if (status < 0)
  {
    close(_sock);
    _sock = -1;
    throw_runtime_error("bind: ", strerror(errno));
  }
}

UDPBCSubscriber::~UDPBCSubscriber()
{
  if (_sock >= 0)
    close(_sock);
  _sock = -1;
}

bool UDPBCSubscriber::is_buffer_empty()
{
  fd_set rfds;
  FD_ZERO(&rfds);
  FD_SET(_sock, &rfds);
  timeval tv = {};
  return select(_sock + 1, &rfds, nullptr, nullptr, &tv) == 0;
}

int UDPBCSubscriber::get_last(void* data, int sz)
{
  int n = 0;
  while (!is_buffer_empty())
  {
    n = recv(_sock, data, sz, MSG_DONTWAIT);
    if (n <= 0)
      return n;
  }
  return n;
}

int UDPBCSubscriber::read_next(void* data, int sz, bool blocking)
{
  int n = recv(_sock, data, sz, 0);
  return n;
}
